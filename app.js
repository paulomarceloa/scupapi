var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var mentions = require('./routes/mentions');
var monitoramentos = require('./routes/monitoramentos');

var https = require('http');

var app = express();



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
//app.use('/mentions',mentions);
app.get('/mentions/:mention_id',function(req,res){
  var optionsget = {
  host : 'localdev.scup.com', 
  path : '/1.1/mentions/'+req.params.mention_id+'&time=1441988101&publickey=JiI6zmKz&signature=c112d506532365ec34fb01f95cf946d1', 
  method : 'GET' 
  };

  console.info('Options prepared:');
  console.info(optionsget);
  console.info('Do the GET call');

  var reqGET = https.get(optionsget, function(res) {
  console.log("statusCode: ", res.statusCode);
  console.log("headers: ", res.headers);


  res.on('data', function(d) {
      console.info('GET result:\n');
      process.stdout.write(d);
      console.info('\n\nCall completed');
    });

  });
})


app.get('/sac/:monitoramento_id',function(req,res){
  var optionsget = {
  host : 'localdev.scup.com', 
  path : '/1.1/tickets/'+req.params.monitoramento_id+'&time=1441988101&publickey=JiI6zmKz&signature=c112d506532365ec34fb01f95cf946d1', 
  method : 'GET' 
  };

  console.info('Options prepared:');
  console.info(optionsget);
  console.info('Do the GET call');

  var reqGET = https.get(optionsget, function(res) {
  console.log("statusCode: ", res.statusCode);
  console.log("headers: ", res.headers);


  res.on('data', function(d) {
      console.info('GET result:\n');
      process.stdout.write(d);
      console.info('\n\nCall completed');
    });

  });
})



app.get('/resposta/:monitoramento_id/:mention_id/:resposta/:conta',function(req,res){
  var optionsget = {
  host : 'localdev.scup.com', 
  path : '/1.1/replymention/'+req.params.monitoramento_id+'/?mention='+req.params.mention_id+'&message='+req.params.resposta+'&account='+req.params.conta+'&time=1441988101&publickey=JiI6zmKz&signature=c112d506532365ec34fb01f95cf946d1', 
  method : 'GET' 
  };

  console.info('Options prepared:');
  console.info(optionsget);
  console.info('Do the GET call');

  var reqGET = https.get(optionsget, function(res) {
  console.log("statusCode: ", res.statusCode);
  console.log("headers: ", res.headers);


  res.on('data', function(d) {
      console.info('GET result:\n');
      process.stdout.write(d);
      console.info('\n\nCall completed');
    });

  });
})
  
  
  
  

app.use('/monitoramentos',monitoramentos);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
